var app = angular.module('App', ['ui.bootstrap', 'ngRoute']);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "list.html",
            controller: "MoviesCtrl"
        })
        .when("/movie/:movieId", {
            templateUrl: "movie.html",
            controller: "MovieCtrl"
        });
});