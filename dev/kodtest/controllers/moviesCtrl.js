app.controller("MoviesCtrl", function($scope, moviesService) {
    $scope.countries = [];
    $scope.genres = [];
    $scope.writers = [];
    var countryArray = [];
    var genreArray = [];
    var writerArray = [];
    $scope.filter = {};
    $scope.max = 10;
    $scope.isReadonly = true;
    $scope.ratingStates = [
        { stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle' },
        { stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty' },
        { stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle' },
        { stateOn: 'glyphicon-heart' },
        { stateOff: 'glyphicon-off' }
    ];
    $scope.IsCountryVisible = false;
    $scope.IsGenreVisible = false;
    $scope.IsWriterVisible = false;

    $scope.ShowHideCountry = function() {
        $scope.IsCountryVisible = $scope.IsCountryVisible ? false : true;
    };
    $scope.ShowHideGenre = function() {
        $scope.IsGenreVisible = $scope.IsGenreVisible ? false : true;
    };
    $scope.ShowHideWriter = function() {
        $scope.IsWriterVisible = $scope.IsWriterVisible ? false : true;
    };

    moviesService.async().then(function(d) {
        $scope.movies = d.Movies;
        $scope.movies.forEach(function(value, i) {

            // Country list for filter
            var countryString = value.Country;
            var countryValue = countryString.split(",").map((item) => item.trim());
            countryValue.concat(countryValue);
            countryArray.push(countryValue);

            // Genre list for filter
            var genreString = value.Genre;
            var genreValue = genreString.split(",").map((item) => item.trim());
            genreValue.concat(genreValue);
            genreArray.push(genreValue);

            // writer list for filter
            var writerString = value.Writer;
            var writerValue = writerString.split(",").map((item) => item.trim());
            writerValue.concat(writerValue);
            writerArray.push(writerValue);

        });

        $scope.countries = [].concat.apply([], countryArray);
        $scope.countries = $scope.countries.filter(function(elem, pos) { return $scope.countries.indexOf(elem) == pos; });

        $scope.genres = [].concat.apply([], genreArray);
        $scope.genres = $scope.genres.filter(function(elem, pos) { return $scope.genres.indexOf(elem) == pos; });

        $scope.writers = [].concat.apply([], writerArray);
        $scope.writers = $scope.writers.filter(function(elem, pos) { return $scope.writers.indexOf(elem) == pos; });

    });

});