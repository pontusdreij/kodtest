app.controller("MovieCtrl", function($scope, moviesService, $routeParams, $filter) {
    $scope.params = $routeParams;

    moviesService.async().then(function(d) {
        $scope.movies = d.Movies;
        var movies = $scope.movies;

        $scope.movie = movies.filter(function(movie) {
            return movie.imdbID === $routeParams.movieId;
        })[0];

    });


});